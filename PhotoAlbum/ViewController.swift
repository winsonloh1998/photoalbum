//
//  ViewController.swift
//  PhotoAlbum
//
//  Created by Daniel Wong on 07/07/2020.
//  Copyright © 2020 Winson. All rights reserved.
//

import UIKit
import Photos
import Hero

struct InternetPhoto: Decodable {
    let download_url: String
}

class CustomCollectionViewCell: UICollectionViewCell{
    @IBOutlet weak var imageView: UIImageView!
}

class ViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    //UIImage Array storing all the device's images
    var imageArray = [UIImage]()
    
    //ImageURL Array storing all the respond Image URL
    var imageUrlArray = [InternetPhoto]()
    
    var width: CGFloat!
    
    override func viewDidLoad() {
        
        //Check Screen Orientation First
        if UIDevice.current.orientation.isLandscape {
            //Get Landscape Screen Size and Devide into 8 Cell
            width = UIScreen.main.bounds.width / 8 - 1
        }else if UIDevice.current.orientation.isPortrait {
            //Get Portrait Screen Size and Devide into 4 Cell
            width = UIScreen.main.bounds.width / 4 - 1
        }
        
        //grabLocalPhotos()
        grabInternetPhotos()
    }
    
    func grabInternetPhotos(){
        //Grab photos over Http Request
        let jsonUrlString = "https://picsum.photos/v2/list"
        let url = URL(string: jsonUrlString)!
        
        //Get the response
        URLSession.shared.dataTask(with: url){
            (data, response, error) in
            
            guard let responseData = data else { return }
            
            do{
                let internetPhotos = try JSONDecoder().decode([InternetPhoto].self, from: responseData)
                
                self.imageUrlArray = internetPhotos
                
                DispatchQueue.main.sync(execute: {
                    //Back to main thread and Reload the image
                    self.collectionView?.reloadData()
                })
                
            }catch let jsonError{
                print("Error Serializing:", jsonError)
            }
        }.resume()
    }
    
    func grabLocalPhotos(){
        //Grab photos from the device
        let imgManager = PHImageManager.default()
        
        // Configure the request Options
        let requestOptions = PHImageRequestOptions()
        requestOptions.isSynchronous = true
        requestOptions.deliveryMode = .highQualityFormat
        
        //Configure the fetch Options
        let fetchOptions = PHFetchOptions()
        fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
        
        
        // Get the result by passing in the fetch Options and the Type (Image)
        let fetchResult : PHFetchResult = PHAsset.fetchAssets(with: .image, options: fetchOptions)
        
        if fetchResult.count > 0{
            //Device has the images within it
            for i in 0..<fetchResult.count{
                imgManager.requestImage(for: fetchResult.object(at: i),
                                        targetSize: CGSize(width: 200, height: 200),
                                        contentMode: .aspectFit,
                                        options: requestOptions, resultHandler: {image, error in
                    //Loop through and save into array
                    self.imageArray.append(image!)
                })
            }
        }else{
            //Device does not has image within it
            print("You got no photos!")
            self.collectionView?.reloadData()
        }
        
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
    
        if UIDevice.current.orientation.isLandscape{
            //Rotate to Landscape is Detected
            width = size.width / 8 - 1
            self.collectionView?.clearsContextBeforeDrawing = true
            self.collectionView?.reloadData()
        }else if UIDevice.current.orientation.isPortrait{
            //Rotate to Potrait is Detected
            width = size.width / 4 - 1
            self.collectionView?.clearsContextBeforeDrawing = true
            self.collectionView?.reloadData()
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //to specify the amount of image it will be displayed on the collection view
        
        //return imageArray.count
        
        return imageUrlArray.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        //Custom or Design the Collection View Cell
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! CustomCollectionViewCell
        

        //cell.imageView.image = imageArray[indexPath.row]
        
        //Convert to Data in order to display URL image
        let imageUrl = URL(string: imageUrlArray[indexPath.row].download_url)!
        let data = try? Data(contentsOf: imageUrl)

        if let imageData = data{
            cell.imageView.image = UIImage(data: imageData)
            cell.imageView.heroID = String(indexPath.row)
            imageArray.append(cell.imageView.image!)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        //Set the size of the cell
        
        return CGSize(width: width, height: width)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        //Set the margin of the cell between each other
        return 1.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        
        //Set the padding of the cell
        return 1.0
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //Delegate or callback(Android) when one of the cell is tapped, pressed or clicked
        
        print(indexPath.row)
        
        let fullScreenViewController = storyboard?.instantiateViewController(identifier: "FullScreenImageViewController") as? FullScreenImageViewController

        fullScreenViewController?.displayingImage = imageArray[indexPath.row]
        fullScreenViewController?.heroID = String(indexPath.row)

        self.navigationController?.pushViewController(fullScreenViewController!, animated: true)
        
        //self.navigationController?.present(fullScreenViewController!, animated: true, completion: nil)
    }
}


