//
//  FullScreenImageViewController.swift
//  PhotoAlbum
//
//  Created by Daniel Wong on 07/07/2020.
//  Copyright © 2020 Winson. All rights reserved.
//

import UIKit
import Hero

class FullScreenImageViewController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    
    var displayingImage: UIImage!
    var heroID: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageView.image = displayingImage
        imageView.heroID = heroID
    }

}
