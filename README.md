# Photo Album iOS13
A simple photo album iOS application.

## What I Have Learned
- UICollectionView
- JSON Decode, API Calling
- UICollectionViewDelegateFlowLayout (display 4 in potrait and 8 in landscape organizely)
- Hero Attribute
- Navigation Controller

## Screenshot - In Portrait
![In Portrait Screenshot](https://bitbucket.org/winsonloh1998/photoalbum/raw/39f8d75c83b3aecfa1a3019002575f12b7221ff6/Screenshot/Portrait.png)

## Screenshot - In Landscape
![In Landscape Screenshot](https://bitbucket.org/winsonloh1998/photoalbum/raw/39f8d75c83b3aecfa1a3019002575f12b7221ff6/Screenshot/Landscape.png)

## Screenshot - Full Image
![Full Image Screenshot](https://bitbucket.org/winsonloh1998/photoalbum/raw/39f8d75c83b3aecfa1a3019002575f12b7221ff6/Screenshot/Image.png)